﻿using BalanceStory.Interfaces;
using System;
using System.Collections.Generic;

namespace BalanceStory.Test.Stubs
{
    public class CalculationHistoryServiceStub : ICalculationHistoryService
    {
        private readonly HistoryStub HistoryStub;

        public CalculationHistoryServiceStub(IDictionary<DateTime, double> uncalculatedFees)
        {
            HistoryStub = new HistoryStub(uncalculatedFees);
        }

        public IHistory RetrieveHistory(IService service)
        {
            return HistoryStub;
        }

        public void VerifyAppliedSum(double expectedSum)
        {
            HistoryStub.VerifyAppliedSum(expectedSum);
        }
    }
}
