﻿using BalanceStory.Interfaces;
using System.Collections.Generic;

namespace BalanceStory.Test.Stubs
{
    public class MultiTariffServiceStub : IService
    {
        private readonly IList<ITariff> Tariffs;

        public MultiTariffServiceStub(IList<ITariff> tariffs)
        {
            Tariffs = tariffs;
        }

        public IList<ITariff> GetTariffs()
        {
            return Tariffs;
        }
    }
}
