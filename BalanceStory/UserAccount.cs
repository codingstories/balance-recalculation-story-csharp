﻿using BalanceStory.Interfaces;
using System;
using System.Collections.Generic;

namespace BalanceStory
{
    public class UserAccount
    {
        private const long epochTimestamp = 0;
        private const double unitRate = 0.8;

        private IBalance balance;

        private IList<DateTime> paymentDates;

        private IList<IService> services;

        private ICalculationHistoryService calculationHistoryService;

        public void RecalculateBalance()
        {
            foreach (IService service in services)
            {
                RecalculateService(service);
            }
        }

        private void RecalculateService(IService service)
        {
            IHistory history = calculationHistoryService.RetrieveHistory(service);
            PayTariff(history, GetHighestTariff(service, history));
        }

        private void PayTariff(IHistory history, double highestTariff)
        {
            history.ApplyStory(highestTariff, unitRate);
            balance.UpdateBalance(highestTariff);
        }

        private double GetHighestTariff(IService service, IHistory history)
        {
            IList<ITariff> tariffs = service.GetTariffs();
            var highestTariff = 0.0;

            foreach (ITariff tariff in tariffs)
            {
                highestTariff = Math.Max(highestTariff, CalculateUnapplied(tariff, history.GetAllFees(tariff, service)));
            }

            return highestTariff;
        }
        
        private double CalculateUnapplied(ITariff tariff, IDictionary<DateTime, double> fees)
        {
            var sum = 0.0;
            foreach (var date in fees)
            {
                if (date.Key > GetLastCalculationDate())
                {
                    sum += date.Value * GetRate(tariff) + tariff.GetAdditionalFee();
                }
            }

            return sum;
        }

        private DateTime GetLastCalculationDate()
        {
            long latest = epochTimestamp;
            foreach (DateTime p in paymentDates)
            {
                latest = Math.Max(p.Ticks, latest);
            }

            return new DateTime(latest);
        }

        private static double GetRate(ITariff tariff)
        {
            return (tariff.GetType().IsUnitBased()) ? unitRate : 1;
        }

        public void SetCalculationHistoryService(ICalculationHistoryService calculationHistoryService)
        {
            this.calculationHistoryService = calculationHistoryService;
        }

        public void SetServices(IList<IService> services)
        {
            this.services = services;
        }

        public void SetBalance(IBalance balance)
        {
            this.balance = balance;
        }

        public void SetPaymentDates(IList<DateTime> paymentDates)
        {
            this.paymentDates = paymentDates;
        }
    }
}
