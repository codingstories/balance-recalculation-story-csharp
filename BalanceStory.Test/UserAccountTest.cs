using BalanceStory.Interfaces;
using BalanceStory.Test.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace BalanceStory.Test
{
    [TestClass]
    public class UserAccountTest
    {
        public const double UnitRate = 0.8;

        private static readonly IList<DateTime> PaymentDates = new List<DateTime>
        {
            new DateTime(2001, 2, 22),
            new DateTime(2001, 1, 23),
            new DateTime(2001, 4, 19)
        };

        private static readonly IDictionary<DateTime, double> EmptyUncalculatedFees = new Dictionary<DateTime, double>()
            ;

        private static readonly IDictionary<DateTime, double> SingleUncalculatedFees = new Dictionary<DateTime, double>
        {
            {new DateTime(2001, 4, 20), 200.0}
        };

        private static readonly IDictionary<DateTime, double> DoubleUncalculatedFees = new Dictionary<DateTime, double>
        {
            {new DateTime(2001, 4, 20), 200.0},
            {new DateTime(2001, 5, 22), 150.0}
        };

        private static readonly Dictionary<DateTime, double> AnotherDoubleUncalculatedFees = new Dictionary<DateTime, double>
        {
            {new DateTime(2001, 6, 25), 120.0},
            {new DateTime(2001, 5, 25), 180.0}
        };

        private readonly UserAccount UserAccount = new UserAccount();
        private readonly BalanceSpy BalanceSpy = new BalanceSpy();

        [TestInitialize]
        public void Init()
        {
            UserAccount.SetPaymentDates(PaymentDates);
            UserAccount.SetBalance(BalanceSpy);
        }

        private void SetupServices(params IService[] services)
        {
            UserAccount.SetServices(services);
        }

        private static MultiTariffServiceStub GetMultiTariffServiceStub(params ITariff[] tariffs)
        {
            return new MultiTariffServiceStub(tariffs);
        }

        private CalculationHistoryServiceStub SetupCalculationHistoryService(CalculationHistoryServiceStub calculationHistoryServiceStub)
        {
            UserAccount.SetCalculationHistoryService(calculationHistoryServiceStub);
            return calculationHistoryServiceStub;
        }

        private void VerifyUpdatedSum(double expectedSum, CalculationHistoryServiceStub calculationHistoryServiceStub)
        {
            BalanceSpy.VerifyUpdatedSum(expectedSum);
            calculationHistoryServiceStub.VerifyAppliedSum(expectedSum);
        }

        [TestMethod]
        public void ShouldNotApplyPayment_When_AllFeesAlreadyRecalculated()
        {
            //given
            SetupServices(new ServiceStub());
            var calculationHistoryServiceStub = SetupCalculationHistoryService(new CalculationHistoryServiceStub(EmptyUncalculatedFees));

            //when
            UserAccount.RecalculateBalance();

            //then
            VerifyUpdatedSum(0.0, calculationHistoryServiceStub);
        }

        [TestMethod]
        public void ShouldApply_SumOfAll_NotCalculatedFees()
        {
            //given
            SetupServices(new ServiceStub());
            var calculationHistoryServiceStub = SetupCalculationHistoryService(new CalculationHistoryServiceStub(DoubleUncalculatedFees));

            //when
            UserAccount.RecalculateBalance();

            //then
            VerifyUpdatedSum(350.0 * UnitRate, calculationHistoryServiceStub);
        }

        [TestMethod]
        public void ShouldApplySum_ForTariff_WithHighestRate()
        {
            //given
            SetupServices(GetMultiTariffServiceStub(new UnitBasedTariffStub(), new NoRateTariffStub()));
            var calculationHistoryServiceStub = SetupCalculationHistoryService(new CalculationHistoryServiceStub(DoubleUncalculatedFees));

            //when
            UserAccount.RecalculateBalance();

            //then
            VerifyUpdatedSum(350.0, calculationHistoryServiceStub);
        }

        [TestMethod]
        public void ShouldApplySum_ForTariff_WithAdditionalFee_ForEachUncalculatedFee()
        {
            //given
            SetupServices(GetMultiTariffServiceStub(new NoRateTariffStub(10.0), new NoRateTariffStub()));
            var calculationHistoryServiceStub = SetupCalculationHistoryService(new CalculationHistoryServiceStub(DoubleUncalculatedFees));

            //when
            UserAccount.RecalculateBalance();

            //then
            VerifyUpdatedSum(350.0 + 10.0 + 10.0, calculationHistoryServiceStub);
        }

        [TestMethod]
        public void ShouldApplySum_ForTariff_WithAdditionalFee_WhenItsHigherThanOtherTariff()
        {
            //given
            SetupServices(GetMultiTariffServiceStub(new NoRateTariffStub(), new UnitBasedTariffStub(50.0)));
            var calculationHistoryServiceStub = SetupCalculationHistoryService(new CalculationHistoryServiceStub(SingleUncalculatedFees));

            //when
            UserAccount.RecalculateBalance();

            //then
            VerifyUpdatedSum(200.0 * UnitRate + 50.0, calculationHistoryServiceStub);
        }

        [TestMethod]
        public void ShouldApplySum_ForTariff_WithHighestRate_WhenItsHigherThanOtherTariff()
        {
            //given
            SetupServices(GetMultiTariffServiceStub(new NoRateTariffStub(), new UnitBasedTariffStub(10.0)));
            var calculationHistoryServiceStub = SetupCalculationHistoryService(new CalculationHistoryServiceStub(SingleUncalculatedFees));

            //when
            UserAccount.RecalculateBalance();

            //then
            VerifyUpdatedSum(200.0, calculationHistoryServiceStub);
        }

        [TestMethod]
        public void ShouldApplySumOfAll_NotCalculatedFees_ForAllServices()
        {
            //given
            SetupServices(new ServiceStub(), new SecondServiceStub());

            var calculationHistoryService = new CalculationHistoryForMultiServiceStub(SingleUncalculatedFees, AnotherDoubleUncalculatedFees);
            UserAccount.SetCalculationHistoryService(calculationHistoryService);

            //when
            UserAccount.RecalculateBalance();

            //then
            BalanceSpy.VerifyUpdatedSum((200.0 + 120.0 + 180.0) * UnitRate);
            calculationHistoryService.VerifyAppliedSumForService(200.0 * UnitRate, typeof(ServiceStub));
            calculationHistoryService.VerifyAppliedSumForService((120.0 + 180.0) * UnitRate, typeof(SecondServiceStub));
        }
    }
}
