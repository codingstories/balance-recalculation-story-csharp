﻿namespace BalanceStory.Interfaces
{
    public interface ITariff
    {
        ITariffType GetType();
        double GetAdditionalFee();
    }
}
