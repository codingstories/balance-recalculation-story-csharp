﻿using BalanceStory.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BalanceStory.Test.Stubs
{
    public class BalanceSpy : IBalance
    {
        private double updatedSum;

        public void UpdateBalance(double sum)
        {
            updatedSum += sum;
        }

        public void VerifyUpdatedSum(double expectedSum)
        {
            Assert.AreEqual(expectedSum, updatedSum, HistoryStub.Delta);
        }
    }
}
