﻿namespace BalanceStory.Interfaces
{
    public interface IBalance
    {
        void UpdateBalance(double sum);
    }
}
