﻿using BalanceStory.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace BalanceStory.Test.Stubs
{
    public class HistoryStub : IHistory
    {
        internal const double Delta = 0.0001;

        private readonly IDictionary<DateTime, double> Fees = new Dictionary<DateTime, double>
        {
            {new DateTime(2001, 3, 28), 100.0},
            {new DateTime(2001, 4, 18), 150.0}
        };

        private double appliedSum;

        public HistoryStub(IDictionary<DateTime, double> uncalculatedFees)
        {
            foreach (var fee in uncalculatedFees)
            {
                Fees[fee.Key] = fee.Value;
            }
        }

        public IDictionary<DateTime, double> GetAllFees(ITariff tariff, IService service)
        {
            return Fees;
        }

        public void ApplyStory(double value, double unitRate)
        {
            appliedSum = value;
            Assert.AreEqual(UserAccountTest.UnitRate, unitRate, Delta);
        }

        public void VerifyAppliedSum(double expectedSum)
        {
            Assert.AreEqual(expectedSum, appliedSum, Delta);
        }
    }
}
