﻿using BalanceStory.Interfaces;

namespace BalanceStory.Test.Stubs
{
    public class UnitBasedTariffStub : ITariff
    {
        private readonly double AdditionalFee;

        public UnitBasedTariffStub()
        {
            AdditionalFee = 0;
        }

        public UnitBasedTariffStub(double additionalFee)
        {
            AdditionalFee = additionalFee;
        }

        public double GetAdditionalFee()
        {
            return AdditionalFee;
        }

        ITariffType ITariff.GetType()
        {
            return new UnitBasedTariffTypeStub();
        }

        private class UnitBasedTariffTypeStub : ITariffType
        {
            public bool IsUnitBased()
            {
                return true;
            }
        }
    }
}
