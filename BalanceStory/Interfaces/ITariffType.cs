﻿namespace BalanceStory.Interfaces
{
    public interface ITariffType
    {
        bool IsUnitBased();
    }
}
