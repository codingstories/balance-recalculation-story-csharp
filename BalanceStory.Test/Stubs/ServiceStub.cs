﻿using BalanceStory.Interfaces;
using System.Collections.Generic;

namespace BalanceStory.Test.Stubs
{
    public class ServiceStub : IService
    {
        private readonly IList<ITariff> Tariffs = new List<ITariff> { new UnitBasedTariffStub() };

        public IList<ITariff> GetTariffs()
        {
            return Tariffs;
        }
    }
}
