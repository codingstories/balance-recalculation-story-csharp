﻿using System;
using System.Collections.Generic;

namespace BalanceStory.Interfaces
{
    public interface IHistory
    {
        IDictionary<DateTime, double> GetAllFees(ITariff tariff, IService service);
        void ApplyStory(double value, double unitRate);
    }
}
