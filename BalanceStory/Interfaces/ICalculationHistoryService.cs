﻿using BalanceStory.Interfaces;

public interface ICalculationHistoryService
{
    IHistory RetrieveHistory(IService service);
}
