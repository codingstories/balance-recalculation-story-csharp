﻿using BalanceStory.Interfaces;

namespace BalanceStory.Test.Stubs
{
    public class NoRateTariffStub : ITariff
    {
        private readonly double AdditionalFee;

        public NoRateTariffStub()
        {
            AdditionalFee = 0;
        }

        public NoRateTariffStub(double additionalFee)
        {
            AdditionalFee = additionalFee;
        }

        public double GetAdditionalFee()
        {
            return AdditionalFee;
        }

        ITariffType ITariff.GetType()
        {
            return new NoRateTariffTypeStub();
        }

        private class NoRateTariffTypeStub : ITariffType
        {
            public bool IsUnitBased()
            {
                return false;
            }
        }
    }
}
