﻿using System.Collections.Generic;

namespace BalanceStory.Interfaces
{
    public interface IService
    {
        IList<ITariff> GetTariffs();
    }
}
