﻿using BalanceStory.Interfaces;
using System;
using System.Collections.Generic;

namespace BalanceStory.Test.Stubs
{
    public class CalculationHistoryForMultiServiceStub : ICalculationHistoryService
    {
        private readonly IDictionary<Type, HistoryStub> HistoryMap = new Dictionary<Type, HistoryStub>();

        public CalculationHistoryForMultiServiceStub(IDictionary<DateTime, double> uncalculatedFees, IDictionary<DateTime, double> secondUncalculatedFees)
        {
            HistoryMap.Add(typeof(ServiceStub), new HistoryStub(uncalculatedFees));
            HistoryMap.Add(typeof(SecondServiceStub), new HistoryStub(secondUncalculatedFees));
        }

        public IHistory RetrieveHistory(IService service)
        {
            return HistoryMap[service.GetType()];
        }

        public void VerifyAppliedSumForService(double expectedSum, Type serviceClass)
        {
            HistoryMap[serviceClass].VerifyAppliedSum(expectedSum);
        }
    }
}
