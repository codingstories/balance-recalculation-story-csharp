# Balance Recalculation Story

**To read**: [https://refactoringstories.com/file-view/https:%2F%2Fgit.epam.com%2FRefactoring-Stories%2Fbalance-recalculation-story-csharp]

## Story Outline
This story has one not obvious function which applies some balance recalculation. This refactoring is about functions, about its size and number of arguments.

## Story Organization
**Story Branch**: master
>`git checkout master`

**Practical task tag for self-study**:task
>`git checkout task`

Tags: #clean_code